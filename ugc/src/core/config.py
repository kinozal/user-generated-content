import os
from pydantic import BaseSettings


class Base(BaseSettings):
    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'
        arbitrary_types_allowed = True


class BrokerSettings(Base):
    host: str = os.getenv('KAFKA_BROKER_HOST', 'broker')
    port: str = os.getenv('KAFKA_BROKER_PORT', 29092)

    class Config:
        env_prefix = 'kafka_'


class Settings(Base):

    PROJECT_NAME: str = os.getenv('PROJECT_NAME', 'UGC')
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    AUTH_VALIDATION_URL: str = os.getenv('AUTH_URL', 'http://ugc:83') + "/validate"

    broker: BrokerSettings = BrokerSettings()


settings = Settings()
