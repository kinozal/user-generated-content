"""Service constants"""

# AUTH ROLES
ACCESS_GUEST = "guest"
ACCESS_USER = "user"
ACCESS_EDITOR = "editor"
ACCESS_ADMIN = "admin"

# MESSAGES
MSG_NOT_AUTHENTICATED = "Not Authenticated"
MSG_PRODUCE_TIMEOUT = "produce timeout"
