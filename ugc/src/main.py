import asyncio
import http
import json
from functools import wraps

import aiohttp
from aiohttp import ClientConnectionError
from aiokafka import AIOKafkaProducer
from aiokafka.errors import KafkaError, KafkaTimeoutError
from fastapi import FastAPI, HTTPException, Request
from pydantic import BaseModel
from time import time
from core.config import settings

from ugc.src.core import const


def authenticate_user(func):
    @wraps(func)
    async def wrapper(*args, request: Request, **kwargs):
        auth_header = request.headers.get("Authorization")
        headers = {"Authorization": auth_header}
        result_role = const.ACCESS_GUEST
        user_id = ""

        try:
            async with aiohttp.ClientSession() as session:
                async with session.post(settings.AUTH_VALIDATION_URL,
                                        headers=headers) as resp:
                    content = await resp.content.read()
                    content_json = json.loads(content.decode())
                    verified = content_json.get("verified")
                    if verified:
                        result_role = content_json.get("role", const.ACCESS_GUEST)
                        user_id = content_json.get("user_id")
        except ClientConnectionError:  # in case of /auth service unavailable
            result_role = const.ACCESS_GUEST
            user_id = ""

        kwargs['role'] = result_role
        kwargs['user_id'] = user_id
        return await func(*args, request, **kwargs)

    return wrapper


app = FastAPI(
    title=settings.PROJECT_NAME,
    docs_url='/ugc/docs',
    openapi_url='/ugc/openapi.json',
)

loop = asyncio.get_running_loop()

producer = AIOKafkaProducer(
    loop=loop,
    client_id='PROJECT_NAME',
    bootstrap_servers=f'{settings.broker.host}:{settings.broker.port}'
)


@app.on_event("startup")
async def startup_event():
    await producer.start()


@app.on_event("shutdown")
async def shutdown_event():
    await producer.stop()


class Item(BaseModel):
    film_id: str
    time_stamp: int


@app.post("/marker")
@authenticate_user
async def create_item1(request: Request, item: Item, user_id: str = "", role: str = const.ACCESS_GUEST):
    if not user_id:
        raise HTTPException(status_code=http.HTTPStatus.UNAUTHORIZED, detail=const.MSG_NOT_AUTHENTICATED)
    request_data = item.dict()
    request_data["user_id"] = user_id
    try:
        data = json.dumps(request_data).encode('utf-8')
        await producer.send_and_wait('marker', data)
        return {"time_stamp": time()}
    except KafkaTimeoutError:
        raise HTTPException(status_code=http.HTTPStatus.INTERNAL_SERVER_ERROR, detail=const.MSG_PRODUCE_TIMEOUT)
    except KafkaError as ex:
        raise HTTPException(status_code=http.HTTPStatus.INTERNAL_SERVER_ERROR, detail=ex.args[0].str())
