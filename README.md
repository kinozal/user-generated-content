# UGC

#### user-generated content

![alt text](img/diagram.png)

В проекте реализована логика генерации контента пользователями.\
1. Реализованна http ручка следящая за моментом текущего просмотра видеофайла.
2. Через встроенный механизм Kafka Engine, Clickhouse подключается к Kafka и берет необходимые для аналитических отчетов данные.

### Используется:
- [traefik](https://traefik.io/)
- [kafka (confluentinc)](https://github.com/confluentinc/cp-all-in-one)
- [fastapi](https://fastapi.tiangolo.com/)
- [clickhouse](https://clickhouse.com/)
- [docker](https://www.docker.com/)

### Краткое описание 
Используя связку `traefik` + `fastapi` обрабатываем входящие запросы\
(описание допустимых запросов можно посмотреть [http://localhost/ugc/doc](http://localhost/ugc/docs)) 
Передаем данные в `kafka` к которой на соответствующе топики подписан `clickhouse`\
Также на интересующие топики возможно подписаться использую `workers` (пример в одноименной папке)

### Выбор БД
Обоснование выбранной базы данных - ClickHouse - и методика по которой производили сравнение находятся в каталоге benchtest.

## Запуск
Находясь в корневой директории проекта выполнить команду

`docker-compose up --build -d`

## Проверка работы
Для того, чтобы убедиться, что все работает правильно можно:
1. Через ручку документации сгенерировать несколько тестовых запростов к сервису
2. Чтобы посмотреть сохраненные данные в таблице в ClickHouse выполнить

docker exec -it clickhouse-node1 clickhouse-client

и далее выполнить запрос

SELECT * FROM movies.marker
