CREATE DATABASE IF NOT EXISTS movies ON CLUSTER company_cluster;

CREATE TABLE IF NOT EXISTS movies.marker_queue
(
  user_id        String,
  film_id        String,
  time_stamp     Int16
)
ENGINE=Kafka('broker:29092', 'marker', 'my-group', 'JSONEachRow') settings  kafka_num_consumers = 1;


CREATE TABLE IF NOT EXISTS movies.marker ON CLUSTER company_cluster
(
  user_id        String,
  film_id        String,
  time_stamp     Int32
)
Engine=MergeTree()
ORDER BY (user_id, film_id, time_stamp);


CREATE MATERIALIZED VIEW IF NOT EXISTS movies.marker_consumer
TO movies.marker
AS SELECT *
FROM movies.marker_queue;
