import csv
import time
from clickhouse_driver import Client


conn = Client('0.0.0.0')
conn.execute("CREATE DATABASE IF NOT EXISTS test")
conn.execute(
    "CREATE TABLE IF NOT EXISTS test.test_table (id TEXT, title TEXT, ingredients TEXT, directions TEXT, "
    "link TEXT, source TEXT, NER TEXT) Engine=MergeTree() ORDER BY id PRIMARY KEY id")


def row_reader():
    with open('./dataset/full_dataset.csv', newline='') as csv_data:
        for line in csv.reader(csv_data, delimiter=','):
            yield line


start = time.time()
for line in row_reader():
    print(line)
conn.execute("INSERT INTO test.test_table VALUES", (line for line in row_reader()))
end = time.time()

result = end - start
print(result)
