import vertica_python
import csv
import time

connection_info = {
    'host': '127.0.0.1',
    'port': 5433,
    'user': 'dbadmin',
    'password': '',
    'database': 'docker',
    'autocommit': True,
}


def row_reader():
    with open('./dataset/full_dataset.csv', newline='') as csv_data:
        for line in csv.reader(csv_data, delimiter=','):
            yield line


with vertica_python.connect(**connection_info) as connection:
    cursor = connection.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS test (id LONG VARCHAR, title LONG VARCHAR, ingredients LONG 
    VARCHAR, directions LONG VARCHAR, link LONG VARCHAR, source LONG VARCHAR, NER LONG VARCHAR);""")
    start = time.time()
    cursor.executemany("INSERT INTO test VALUES (?,?,?,?,?,?,?)", [line for line in row_reader()], use_prepared_statements=False)
    connection.commit()
    result = (time.time() - start)
    print(result)


